resource "aws_instance" "germy" {
  ami           = "ami-09538990a0c4fe9be"
  instance_type = var.instance_type


  tags = {
    name = "test-server"
  }

}

resource "aws_vpc" "name" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "test-vpc"
  }

}

variable "instance_type" {
  description = "instance type"
  type        = string

}